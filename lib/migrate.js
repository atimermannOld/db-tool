/**
 * Created by andre on 22/11/14.
 *
 *
 */
'use strict';


var fs = require('fs'),
    path = require('path'),
    debug = require('debug')('info'),
    util = require('util'),
    _s = require('underscore.string'),
    convertTool = require('./convert-tool'),
    modelTool = require('./model-tool'),
    _ = require('lodash');

require('colors');
require('date-utils');


var Migrate = {

    /**
     * Diretório onde será montado a estrutura do Migrate
     */
    dataDir: 'data',

    /**
     * Diretório onde irá buscar os fontes dentro da dataDir
     */
    sourceDir: 'source',

    /**
     * Diretório onde os arquivos de migração serão criados
     */
    migrationDir: 'migration',

    /**
     * Diretório onde os esquemas serão gerados
     */
    schemaDir: 'schema',


    /**
     * Diretório alvo, normalmente diretório do projeto
     */
    path: '.',

    /**
     * Retorna o caminho completo sourceDir
     */
    get sourcePath() {


        if (!this._sourcePath) {
            this._sourcePath = path.resolve(this.path, this.dataDir, this.sourceDir);
        }

        return this._sourcePath;
    },
    /**
     * Retorna o caminho completo sourceDir
     */
    get migrationPath() {

        if (!this._migrationPath) {
            this._migrationPath = path.resolve(this.path, this.dataDir, this.migrationDir);
        }
        return this._migrationPath;
    },
    /**
     * Retorna o caminho completo sourceDir
     */
    get schemaPath() {

        if (!this._schemaPath) {
            this._schemaPath = path.resolve(this.path, this.dataDir, this.schemaDir);
        }
        return this._schemaPath;
    },

    /**
     * Realiza configuração do módulu através do argv
     * @param argv
     */
    configByArgv: function (argv) {

        // Path
        this.path = argv.path ? argv.path : '.';

        // TODO: Permitir personalizar sourceDir, dataDir, schemaDir e migrationDir, porém a configuração personalizada, deverá ser salva na configuração, para que o script saiba o caminho correto

    },

    /**
     * Inicializa estrutura de diretórios, criar arquivos base
     *
     */
    init: function () {

        var self = this;

        debug('Ininicializando estrutura');

        var target = path.resolve(
            self.path,
            self.dataDir
        );

        debug("Target: '%s'", target);

        ///////////////////////////////////////////////////////////////////////////////
        // Verifica se diretório existe
        ///////////////////////////////////////////////////////////////////////////////
        if (fs.existsSync(target)) {
            throw new Error('Estrutura já foi criada');
        }
        ///////////////////////////////////////////////////////////////////////////////
        // Cria diretório DataBase
        ///////////////////////////////////////////////////////////////////////////////
        debug("Criando '%s'", target);
        fs.mkdirSync(target);

        ///////////////////////////////////////////////////////////////////////////////
        // Cria diretórios Migrations e Schemas
        ///////////////////////////////////////////////////////////////////////////////
        var sourcePath = path.resolve(target, self.sourceDir);
        var migrationPath = path.resolve(target, self.migrationDir);
        var schemaPath = path.resolve(target, self.schemaDir);

        debug("Criando '%s'", sourcePath);
        fs.mkdirSync(sourcePath);

        debug("Criando '%s'", migrationPath);
        fs.mkdirSync(migrationPath);

        debug("Criando '%s'", schemaPath);
        fs.mkdirSync(schemaPath);


        ///////////////////////////////////////////////////////////////////////////////
        // Cria arquivo config.json
        ///////////////////////////////////////////////////////////////////////////////

        // TODO: Alterar para YAML?
        var configPath = path.resolve(target, 'config.json');

        var config = {
            'currentMigration': null
        };

        debug("Criando '%s'", configPath);
        fs.writeFileSync(configPath, JSON.stringify(config));

        console.log("Estrutura criada com sucesso.");


    },

    /**
     * Cria um novo esquema (snapshot) da estrutura do banco de dados no formato: schema-[data]-[numero-sequencial-com-5-digitos]
     *
     * @param file                      Nome do Arquivo
     * @param inputFormat               Formato de Origem (DIA)
     * @param diaOutputFormat           Banco de dados usado (mysql-innodb)
     * @param client                    Cliente de banco de dados. Padrão: mysql (deve ser compatível com diaOutputFormat)
     * @param {boolean} loadInvalid     Gera esquema mesmo com sql não válido
     */
    createSchema: function (file, inputFormat, diaOutputFormat, client, loadInvalid, save) {
        // TODO: Criar objeto de opções pois passou de 4 parâmetros

        var self = this;

        self._validateMigrateDirectory();

        debug("file: '%s'", file);

        //////////////////////////////////////////////////////////////////////////////
        // Verifica se diretório migration/schema existe
        //////////////////////////////////////////////////////////////////////////////
        debug("inputFormat: '%s'", inputFormat);


        //////////////////////////////////////////////////////////////////////////////
        // Verifica se file existe, se não existe tenta carregar no diretório source
        //////////////////////////////////////////////////////////////////////////////


        if (file) {

            //self.sourcePath = 1;

            if (!fs.existsSync(file)) {

                throw new Error(util.format("Arquivo '%s' não encontrado", file));
            }

        } else {

            debug('Procurando em %s', self.sourcePath);

            // Procura versão mais recente
            // TODO: Adicionar suporte a .sql
            var recent = self._getNewer(self.sourcePath, inputFormat);

            if (recent) {
                file = path.resolve(self.sourcePath, recent.file);
            } else {
                throw new Error(util.format("Nenhum arquivo encontrado no diretório '%s'. Deve estar no formato: .*\d{5}(.dia|.sql)", self.sourcePath));
            }
        }

        //////////////////////////////////////////////////////////////////
        // processa
        //////////////////////////////////////////////////////////////////

        // Carrega ultimo ultimo esquema criado para ler revisão ultima revisão
        var recentDir = self._getNewer(self.schemaPath, null);

        // Nova revisão
        var revision = recentDir ? recentDir.revision + 1 : 1;

        debug('Nova revisão: %s', revision);

        // Define
        var schemaTarget = path.join(self.schemaPath, util.format("schema-%s-%s", Date.today().toFormat("YYYYMMDD"), _s.lpad(revision, 5, '0')));
        var schemaFileTarget = path.join(schemaTarget, 'migration-schema.json');

        // Cria diretório


        // Gera um SQL a apartir do dia (criado pela ferramenta parsediasql)
        convertTool.dia2sqlraw(file, diaOutputFormat, function (data) {


            // Converte o SQL gerado para um modelo em json
            if (save) {
                fs.mkdirSync(schemaTarget);
                debug("Criado %s", schemaFileTarget);
            }

            convertTool.sqlraw2modeljson(data, schemaFileTarget, {
                adapter: client,
                loadInvalid: loadInvalid,
                simulate: !save
            });

            if (!save) {
                console.log("Validação finalizada! Use '--save' para criar um schema.".red.bold);
            }


        });


    },

    /**
     * Gera uma nova migração. Se numero-do-schema-algo não for passado, assume mais recente
     * É cria os gabarito para preenchimento, que serão utilizado na migração de dados
     *
     * @param schemaTarget  Id do schema para onde vamos migrar, se null, migra do ultimo
     * @param options
     */
    createMigration: function (schemaTarget, options) {

        options = _.assign({

            // Força novo (NÃO REALIZA MIGRAÇÃO DE DADOS, IGNORA)
            forceNew: false,

            // Se realiza backup antes de executar a migração
            backup: true,

            // Gera SQl em vez de Salvar diretamente na base de dados
            saveSql: false,

            // Cliente onde os dados serão salvos
            client: 'mysql',

            // Usado quando realizado migração entre diferentes base de dados, se null usa o mesmo de client, caso
            // contrário especificar a base de origem
            clientOrigin: null,

            // Cria ou remove base de dados
            drop: false,

            // Configuração do Banco de dados
            host: '127.0.0.1',
            user: null,
            password: null,
            database: null,

            // Configuração do Banco de dados de origem
            hostOrigin: '127.0.0.1',
            userOrigin: null,
            passwordOrigin: null,
            databaseOrigin: null

        }, options);

        // TODO: Permitir salvar os dados de conexão na configuração principal, assim não fica obrigado digitar na próxima migração. Origin continua obrigatório


        var self = this,
            recentSchema,
            recentMigration,
            migrationTarget,
            schemaFile;

        self._validateMigrateDirectory();

        ////////////////////////////////////////////////////////////
        // Carrega Id do Schema para onde vamos migrar
        ////////////////////////////////////////////////////////////
        if (!schemaTarget) {

            // Carrega migração mais recente
            recentSchema = self._getNewer(self.schemaPath, null);

            if (!recentSchema) {

                throw  new Error("Nenhum schema encontrado");
            }

            schemaTarget = recentSchema.revision;

        }

        ////////////////////////////////////////////////////////////
        // Recupera ultima migração
        ////////////////////////////////////////////////////////////

        recentMigration = self._getNewer(self.migrationPath, null);

        if (recentMigration) {
            migrationTarget = recentMigration.revision + 1;
        } else {
            migrationTarget = 1;
        }

        ////////////////////////////////////////////////////////////
        // Define se será uma nova migração ou se será necessario migrar os dados
        ////////////////////////////////////////////////////////////
        var newMigration = !recentMigration || options.forceNew;

        var nameNewMigration = util.format("migration-%s-%s", Date.today().toFormat("YYYYMMDD"), _s.lpad(migrationTarget, 5, '0'));

        ////////////////////////////////////////////////////////////
        // Verifica se a Ultima Migração foi processada
        ////////////////////////////////////////////////////////////

        if (recentMigration) {

            var lastConfig = JSON.parse(fs.readFileSync(path.resolve(self.migrationPath, recentMigration.file, "config.json")));

            if (!lastConfig.migrated) {

                throw new Error("Última migração '" + _s.lpad(recentMigration.revision, 5, '0') + "' não foi processada.");
            }

        }

        ////////////////////////////////////////////////////////////
        // Verifica se Esquema existe
        ////////////////////////////////////////////////////////////
        var schema = self._getFileByRevision(self.schemaPath, null, schemaTarget);

        if (!schema) {
            throw new Error('Esquema \'' + schemaTarget + '\' não existe');
        }


        console.log('Criando migração a partir do schema: ', schema);
        ////////////////////////////////////////////////////////////
        // Cria diretório
        ////////////////////////////////////////////////////////////
        fs.mkdirSync(path.resolve(self.migrationPath, nameNewMigration));


        ////////////////////////////////////////////////////////////
        // Configura Migração
        ////////////////////////////////////////////////////////////
        var config = {

            // Configurações
            options: options,

            // Se é nova migração
            newMigration: newMigration,

            // TODO: Atualizar arqui. Deve rodar a migração
            // Migração de Origem (Deve ser a mesma da atual, usado pra verificação)
            migrationOrigin: null,

            // Migração Alvo
            schemaTarget: schemaTarget,

            // Se migração foi executada
            migrated: false

        };

        // Salva Configuração
        fs.writeFileSync(path.resolve(self.migrationPath, nameNewMigration, "config.json"), JSON.stringify(config));


        ////////////////////////////////////////////////////////////
        // Caso não seja uma nova migração, cria Gabaritos e Templates da Migração
        ////////////////////////////////////////////////////////////

        console.log("Migração criada: %s", nameNewMigration);


    },

    /**
     * Executa última migração.
     *
     *
     */
    runMigration: function (verbose) {

        var self = this;

        self._validateMigrateDirectory();

        ////////////////////////////////////////////////////////////
        // Carrega ultima migração e valida
        ////////////////////////////////////////////////////////////
        var migration = self._getNewer(self.migrationPath, null);

        if (migration) {

            var configPath = path.resolve(self.migrationPath, migration.file, "config.json");

            var config = JSON.parse(fs.readFileSync(configPath));

            if (config.migrated) {
                throw new Error("Migração '" + _s.lpad(migration.revision, 5, '0') + "' já processada.");
            }
        } else {

            throw new Error("Nenhuma migração disponível");

        }
        ////////////////////////////////////////////////////////////

        var schema = self._getFileByRevision(self.schemaPath, null, config.schemaTarget);


        var model = JSON.parse(fs.readFileSync(path.resolve(self.schemaPath, schema, 'migration-schema.json')));

        //console.log(config);

        ////////////////////////////////////////////////////////////
        // BACKUP
        ////////////////////////////////////////////////////////////

        //TODO: Caso o banco de dados antigo existe, realiza backup

        ////////////////////////////////////////////////////////////
        // SALVA DADOS NO FORMATO JSON DO BANCO ATUAL
        ////////////////////////////////////////////////////////////

        // TODO: Realizar exportação de dados

        ////////////////////////////////////////////////////////////
        // MIGRAÇÃO DE ESTRUTURA
        ////////////////////////////////////////////////////////////


        // Configuração do banco de dados caso não seja pra criar sql
        if (!config.options.saveSql) {

            modelTool.connection = {
                host: config.options.host,
                user: config.options.user,
                password: config.options.password,
                database: config.options.database
            }

        }

        modelTool.verbose = verbose;
        modelTool.createDb(model, config.options.drop, function (status) {

            if (status) {


                // Muda Migração como concluido. TODO: Quando for desenvolvido migração de dados mudar aqui


                config.migrated = true;
                fs.writeFileSync(configPath, JSON.stringify(config));

            }


        });


        ////////////////////////////////////////////////////////////
        // MIGRAÇÃO DE DADOS
        ////////////////////////////////////////////////////////////

    },

    /**
     * Retorna o arquivo/Diretorio mais recente dentro de um diretório passado, usando a regra:
     *      <nome>-<revisao_de_5_digitos>.<extension>
     *
     * @param directory
     * @param extension
     * @returns {*} Retorna NULL, ou objeto com file e revision
     * @private
     */
    _getNewer: function (directory, extension) {

        var result,
            recentFile,
            rev,
            recent,
            reFile;

        if (extension) {
            // Concatenando Expressões Regulares
            reFile = new RegExp("-(\\d{5})\\." + extension + "$");
        } else {
            reFile = /-(\d{5})$/;
        }


        fs.readdirSync(directory).forEach(function (file) {

            result = file.match(reFile);

            if (result) {
                rev = parseInt(result[1]);

                if (!recentFile || rev > recent) {
                    recentFile = result.input;
                    recent = rev;
                }
            }
        });

        if (recentFile) {
            return {
                file: recentFile,
                revision: recent
            };
        } else {
            return null
        }


    },

    /**
     * Retorna o nome de um Arquivo versionado baseado na sua revisão
     *
     * @param directory
     * @param extension
     * @param schemaId
     * @returns {null} | {string}  Nome do arquivo
     * @private
     */
    _getFileByRevision: function (directory, extension, schemaId) {


        var match,
            reFile,
            result = null;

        if (extension) {
            reFile = /-(\d{5})\./ + extension + /$/;
        } else {
            reFile = /-(\d{5})$/;
        }

        fs.readdirSync(directory).forEach(function (file) {

            match = file.match(reFile);

            if (match && parseInt(match[1]) === parseInt(schemaId)) {

                result = match.input;
                return false;
            }
        });

        return result;

    },

    /**
     * Valida se estrutura de diretório está correta
     * @private
     */
    _validateMigrateDirectory: function () {

        var self = this;

        if (!fs.existsSync(path.resolve(self.path, self.dataDir, 'config.json'))) {

            throw new Error('Necesário criar estrutura de migração, execute init-migration');
        }


    }

};


module.exports = Migrate;
