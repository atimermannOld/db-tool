/**
 * Created by andre on 23/11/14.
 */

var exec = require('child_process').exec,
    modelValidate = require('./model-validate'),
    sqlParser = require('./sql-parser'),
    fs = require('fs'),
    util = require('util'),
    _ = require("lodash"),
    _s = require("underscore.string");


module.exports = {

    /**
     * Gera SQL RAW, de um arquivo DIA
     *
     * @param client        Motor de rendeziação, padrão: Mysql TODO: implementar outros
     * @param input         Entrada, deve ser o caminho de um arquivo DIA válido
     * @param callback      Callback que recebe um parâmetro com os dados
     */
    dia2sqlraw: function (input, client, callback) {


        // Formato que será renderizado
        client = client ? client : 'mysql-innodb';

        ////////////////////////////////////////////////////////
        // Verifica se Input é um arquivo Válido
        ////////////////////////////////////////////////////////
        var stat = fs.statSync(input);

        if (!stat || !stat.isFile()) {
            throw "Arquivo '" + input + "' inválido.";
        }

        ////////////////////////////////////////////////////////
        // Executa conversão
        ////////////////////////////////////////////////////////
        var child = exec(util.format("parsediasql  --file %s --db %s", input, client), function (err, stdout, stderr) {


            ////////////////////////////////////////////////////////
            // Verifica Erro
            ////////////////////////////////////////////////////////
            if (err) {

                if (err.code == 255){
                    throw new Error("Verifique no diagrama, se existe alguma 'Associoation' desconectado. Normalmente ocorrer quando o componente é movido.");

                }else{
                    throw err;
                }


            }

            ////////////////////////////////////////////////////////
            // Imprime Alerta na Tela
            ////////////////////////////////////////////////////////
            console.log(stderr);

            ////////////////////////////////////////////////////////
            // Se foi passado um callback, retorna resultado
            ////////////////////////////////////////////////////////
            callback(stdout);


        });


    },
    /* Gera SQL RAW, de um arquivo DIA
     *
     * @param input     Entrada, deve ser o caminho de um arquivo DIA válido
     * @param engine    Motor de rendeziação, padrão: Mysql
     * @param output    Nome do caminho de um arquivo para salvar o resultado
     */
    dia2sqlrawSave: function (input, engine, file, callback) {

        var self = this;


        self.dia2sqlraw(input, engine, function (data) {

            fs.writeFile(file, data, function (err) {
                if (err) {
                    throw err;
                } else {
                    console.log("Arquivo '%s' salvo com sucesso.", file);

                    if (callback) {
                        callback();
                    }

                }
            });

        });


    },

    /* Gera SQL RAW, de um arquivo DIA, resultado será impresso na tela
     * @param engine    Motor de rendeziação, padrão: Mysql
     * @param input     Entrada, deve ser o caminho de um arquivo DIA válido
     * @param callback
     */
    dia2sqlrawPrint: function (input, engine, callback) {

        var self = this;

        self.dia2sqlraw(input, engine, function (data) {

            console.log(data);

            if (callback) {
                callback();
            }

        });

    },

    sqlraw2modeljson: function (input, output, options) {

        var self = this;

        options = _.assign({
            adapter: "mysql",
            savePath: null,
            loadInvalid: false,
            // Modo Simulação
            simulate: false
        }, options);


        sqlParser.loadFromString(input, options.adapter, function (modelJson) {


            // Se vai continuar o processamento, use loadInvalid para forçar
            var validate = self.validateModel(modelJson) || options.loadInvalid;


            if (!options.simulate) {
                ////////////////////////////////////////////////////////
                // Se foi passado um callback, retorna resultado
                ////////////////////////////////////////////////////////
                if (output instanceof Function) {

                    if (options.savePath && validate) {
                        self.saveModelJson(options.savePath, modelJson);
                    }

                    output(!validate, modelJson);

                }
                ////////////////////////////////////////////////////////
                // Foi passado uma string, um caminho pra salvar
                ////////////////////////////////////////////////////////
                else if (output && validate) {

                    self.saveModelJson(output, modelJson)

                }
                ////////////////////////////////////////////////////////
                // Imprime na tela
                ////////////////////////////////////////////////////////
                else if (validate) {
                    console.log(JSON.stringify(modelJson));
                }
            }


        });


    },

    /**
     * Gera um SQl a partir de um modeljson
     */
    modeljson2sql: function (input, output) {

        var modelJson;

        // Criar lib própria

        // create-sql
        // model-manager
        // model-tool


        // Irá salvar model no Banco de dados
        // Irá crair SQL (pra diferentes linguagens (SQL, POSTGRES, talvez usar adapter)
        // Fazer backup de dados
        // Converter dados para Json
        // Recuperar dados do Json
        // Verificar diferença
        //


        ////////////////////////////////////////////////////////////////////////
        // Se é um objeto já está pronto
        ////////////////////////////////////////////////////////////////////////
        if (input instanceof Object) {

            modelJson = input;

        } else {

            modelJson = JSON.parse(fs.readFileSync(input));
        }


        var knex = require('knex')({
            client: 'mysql'
        });

        ////////////////////////////////////////////////////////////////////////
        // CRIA TABELAS
        ////////////////////////////////////////////////////////////////////////
        _.forIn(modelJson.tables, function (table, tableName) {

            console.log(tableName);

            var x = knex
                .schema
                .createTable(tableName, function (t) {


                    var a;

                    _.forIn(table.columns, function (column) {

                        if (column.type === 'PRIMARY') {

                            if (table.primaryKey.columns.indexOf(column.title) !== -1) {

                                t.increments(column.title);

                            } else {

                                t.specificType(column.title, 'int unsigned');


                            }


                        }

                        else if (column.typeSize) {

                            a = t.specificType(column.title, util.format("%s (%s)", column.type, column.typeSize));

                            if (column.nullable) {
                                a.nullable;
                            } else {
                                a.notNullable;
                            }

                            if (column.comment) {
                                a.comment(column.comment)
                            }

                            if (column.default) {
                                a.defaultTo(_s.trim(column.default, "'"))
                            }

                        } else {

                            a = t.specificType(column.title, column.type);

                            if (column.nullable) {
                                a.nullable;
                            } else {
                                a.notNullable;
                            }
                            if (column.comment) {
                                a.comment(column.comment)
                            }
                            if (column.default) {
                                a.defaultTo(_s.trim(column.default, "'"))
                            }

                        }


                    });

                    t.primary(["xxx", 'yyy']);
                    t.unique(["a", "b"]);
                    t.index(["c", "d"])


                })
                .toString();


            console.log(x);
            console.log('----------------------------------------------------');


        });


        ////////////////////////////////////////////////////////////////////////
        // Altera Referencias
        ////////////////////////////////////////////////////////////////////////


    },


    /**
     * Salva o Modelo Json
     *
     * @param file
     */
    saveModelJson: function (file, modelJson) {


        fs.writeFileSync(file, JSON.stringify(modelJson));

        //function (err) {
        //    if (err) {
        //        throw(err);
        //    } else {
        console.log("Arquivo '%s' Salvo com sucesso.", file);
        //}

    },


    /**
     * Valida Modelo
     *
     * @param model
     */
    validateModel: function (model) {

        var errorList = {
            error: [],
            warn: [],
            info: []
        };

        var x;

        modelValidate.forEach(function (item) {
            var result = item(model);

            for (x in result.error) {
                errorList.error.push(result.error[x]);
            }

            for (x in result.warn) {
                errorList.warn.push(result.warn[x]);
            }

            for (x in result.info) {
                errorList.info.push(result.info[x]);
            }

        });


        // Só exibe WARN depois de resolver os ERRORS e só exibe info depois de resolver os WARN
        for (x in errorList.error) {
            console.log(errorList.error[x]);
        }

        for (x in errorList.warn) {
            console.log(errorList.warn[x]);
        }

        if (!errorList.warn.length && !errorList.error.length) {
            for (x in errorList.info) {
                console.log(errorList.info[x]);
            }
        }

        return (errorList.error.length === 0)

    }

};