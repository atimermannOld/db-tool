/**
 * Created by andre on 23/11/14.
 */

var path = require('path'),
    fs = require('fs'),
    readline = require('readline'),
    Stream = require('stream');

module.exports = {

    /**
     * Adaptador usado para processar o arquivo
     */
    adapterName: 'mysql',

    /**
     * Buffer Usado Internamente para armazenar pedaço do sql q será enviado para o parser
     *
     */
    buffer: [],


    /**
     * CONSTANTE: MATCH_IN
     * Representa inicio do trecho
     */
    MATCH_IN: 0,

    /**
     * CONSTANTE: MATHC_OUT
     * Representa fim do trecho
     */
    MATCH_OUT: 1,


    /**
     * Armazena informação do parser que combinou ao procurar inicio do trecho, para ser usado posteriormente
     */
    parserMatch: null,

    /**
     * Modo de Procura, inicialmente MATCH_IN (inicio do trecho)
     */
    mode: 0,


    /**
     * Adaptador Carregado
     */
    adapter: null,


    /**
     * Carrega Arquivo SQL
     *
     * @param file      Caminho do arquivo
     * @param adapter   Adaptador a ser usado: Padrão myaql
     * @param cbResult  Callback com o resultado
     */
    loadFromFile: function (file, adapterName, cbResult) {
        'use strict';

        var self = this,
            absFile = path.resolve(file),
            stream;

        self.adapterName = adapterName;

        fs.exists(absFile, function (exists) {

            if (exists) {

                // Gera um Stream
                stream = fs.createReadStream(file);

                self.open(stream, cbResult);
            } else {
                console.log("Arquivo '%s' não encontrado.", file);
            }

        });

        // Gera SQL do diagrama /usr/bin/env parsediasql


    },

    /**
     * Carregar arquivo a partir de uma String
     *
     * @param stream
     * @param adapter
     * @param cbResult
     */
    loadFromString: function (input, adapterName, cbResult) {
        "use strict";

        var self = this;

        self.adapterName = adapterName;

        // Carrega Adaptador
        self.adapter = require("../adapter/" + self.adapterName);

        var lines = input.split('\n');

        // Carrega Linha por linha e processa
        lines.forEach(function (line) {

            self.parseLine(line);

        });


        cbResult(self.adapter);


    },

    /**
     * Abre arquivo e executa linha por linha
     * @param file
     */
    open: function (instream, cbResult) {
        "use strict";

        var self = this;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Processador de Linha
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var s = new Stream();

        var rl = readline.createInterface(instream, s);

        self.adapter = require("./adapter/" + self.adapterName);


        rl.on('line', function (line) {
            self.parseLine(line);
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FINALIZAÇÃO
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        rl.on('close', function () {

            // Fim do Arquivo
            if (self.parserMatch) {
                throw "Erro: Não foi possível fechar o trecho";
            }

            cbResult(self.adapter);


        });

    },


    /**
     * Processa linha Atual
     *
     * @param line
     */
    parseLine: function (line) {
        "use strict";

        var self = this;
        // Alimentamos o buffer
        self.buffer.push(line);

        /////////////////////////////////////////////////////////////////////
        // MODO 1: Busca por alguém que combine o inicio do trecho
        /////////////////////////////////////////////////////////////////////
        if (self.mode === self.MATCH_IN) {


            self.adapter.parsers.forEach(function (parser) {

                // Se combinar, muda par ao modo MATCH_OUT
                if (parser.matchIn(line)) {

                    // Inicia um novo buffer
                    self.buffer = [];
                    self.buffer.push(line);

                    // Muda para modo de busca de fim de trecho
                    self.mode = self.MATCH_OUT;

                    // Define o parser
                    self.parserMatch = parser;
                }


            });


        }

        /////////////////////////////////////////////////////////////////////
        // MODO 2: Busca para encontrar a linha que combina com o fim do trecho
        // Não usei ELSE, pois pode ocorrer de termos MATCHIN e MATCH OUT na mesma linha
        /////////////////////////////////////////////////////////////////////
        if (self.mode === self.MATCH_OUT) {

            // Se combinar definimos o fim do trecho
            if (self.parserMatch.matchOut(line)) {

                // Envia o trecho para o parser processar
                self.parserMatch.parser(self.buffer);


                // Reinicia processo
                // Volta para o modo de busca de inicio de trecho
                self.mode = self.MATCH_IN;

                self.parserMatch = null;

            }

        }


    }

};
