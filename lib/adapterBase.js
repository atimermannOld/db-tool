/**
 * Created by André Timermann on 08/11/14.
 *
 * Objeto base para criar os adaptadores
 *
 * TODO: Ver como funciona o set/get do objeto e utilizar
 *
 * TODO: Criar Objeto para Column, Index e Relation
 */

////////////////////////////////////////////////////////////
// Tabela - Base para propriedade de Tabela
////////////////////////////////////////////////////////////
var tableBase = {
    tableName: '',
    primaryKey: null,
    columns: [],
    index: [],
    relations: [],
    data: [],
    /**
     * Adiciona Uma coluna a tabela
     * @param column
     */
    addColumn: function (column) {
        'use strict';

        //TODO: Criar Validações
        // Validações
        if (!column.title) {
            console.log("ERRO: Título da tabela '%s'");
        }

        this.columns.push(column);

    },

    /**
     * Adiciona um incide
     * @param index
     */
    addIndex: function (index) {
        "use strict";

        //TODO: Criar Validações
        this.index.push(index);
    },

    /**
     * Define a(s) chave(s) primária(s)
     *
     * @param pk
     */
    setPrimaryKey: function (pk) {
        "use strict";

        //TODO: Criar Validações
        this.primaryKey = pk;

    },

    /**
     * Adiciona um Relacionamento
     *
     * @param relation
     */
    addRelation: function (relation) {
        "use strict";

        //TODO: Criar Validações
        this.relations.push(relation);
    },

    /**
     * Adiciona Dados a tabela (geralmente dados para serem carregados inicialmente FIXTURES)
     * @param data
     */
    addData: function (data) {
        "use strict";

        this.data.push(data);
    }
};


////////////////////////////////////////////////////////////
// Adapter
////////////////////////////////////////////////////////////
module.exports = Object.create(Object, {

    /**
     * Lista de Tabelas
     */
    tables: {
        value: {}
    },

    /**
     * Processadores do SQL
     * Cada item deste array deve conter um objeto com 3 funções: matchIn, matchOut e Parser
     *
     * O sql_parser, vai percorrer o sql e pra cada linha vai executar o matchIn, se retornar true marca como inicio do trecho
     * a partir deste momento ele percorre o arquivo procurando a linha que combine com matchOut, se retornar true, marca o fim do trecho
     *
     * O trecho então é enviaro para Parser, de deve realizar o parser do trecho e salvar os dados num objeto
     *
     */
    parsers: {
        value: []
    },

    /**
     * Acessa uma tabela para definir propriedades
     * @param tableName
     */
    table: {
        value: function (tableName) {
            "use strict";

            if (!this.tables[tableName]) {

                this.tables[tableName] = Object.create(tableBase);
                this.tables[tableName].columns = [];
                this.tables[tableName].primaryKey = [];
                this.tables[tableName].index = [];
                this.tables[tableName].data = [];
                this.tables[tableName].relations = [];
                this.tables[tableName].tableName = tableName;

            }

            return this.tables[tableName];
        }
    }

});