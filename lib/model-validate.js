/**
 * Created by André Timermann on 15/11/14.
 *
 * Módulo responsável por validar os modelos criado no SQL
 * TODO: Validar tabelas REPETIDAS, COLUNAS Repetidas (apesar q acho q já tem)
 *
 */
"use strict";

var util = require('util'),
    color = require("colors");

module.exports = [

    /* *****************************************************************************************************************
     * Valida Se foi Criado Primary Key corretamente
     * @param model
     * ****************************************************************************************************************/
    function (model) {

        var result = {error: [], warn: [], info: []};

        for (var tableName in model.tables) {

            var table = model.tables[tableName];

            ////////////////////////////////////////////////////////////////////////////////////////////////
            // Procura pela coluna com o mesmo nome da tabela acrecido de _id
            ////////////////////////////////////////////////////////////////////////////////////////////////
            var found = false;
            var target = tableName + '_id';


            for (var i in table.columns) {

                var column = table.columns[i];

                if (column.title === target) {
                    found = true;

                    /////////////////////////////////////////////
                    // Se encontrou, Valida se o tipo é PRIMARY
                    /////////////////////////////////////////////
                    if (column.type !== 'PRIMARY') {


                        result.warn.push(aUtil.getWarn(
                            "Na tabela '%s', coluna da chave primária '%s' com tipo inválido '%s', deve ser %s!",
                            tableName,
                            column.title,
                            column.type,
                            "PRIMARY"
                        ));

                    }

                    // Validação abaixo não é necessário pois sempre será NOT NULL e por padrão é criado como NULL e
                    // queremos q não seja necessário informar parâemtros, tudo bem ser null no dia , depois trocamos
                    //if (column.nullable !== true) {
                    //
                    //    result.warn.push(aUtil.getWarn(
                    //        "Na tabela '%s' chave primária '%s' deve ser NOT NULL!",
                    //        tableName,
                    //        column.title
                    //    );
                    //
                    //}
                    /////////////////////////////////////////////
                    // Verifica a Chave Primária
                    /////////////////////////////////////////////

                    // verifica se foi criada chave primária

                    if (!table.primaryKey.primaryKeyName) {

                        result.warn.push(aUtil.getWarn(
                            "Tabela '%s' sem a restrição da chave primária",
                            tableName
                        ));

                    } else {

                        var pkName = table.primaryKey.primaryKeyName;
                        var pkIndiceTarget = "pk_" + tableName;

                        if (pkName !== pkIndiceTarget) {

                            result.warn.push(aUtil.getWarn(
                                "Na tabela '%s', restrição da chave primária '%s' com nome inválido '%s', deve ser %s!",
                                tableName,
                                column.title,
                                pkName,
                                pkIndiceTarget
                            ));
                        }
                    }


                    break;
                }

            }

            if (!found) {
                result.warn.push(aUtil.getWarn("Tabela '%s' sem coluna com título '%s'! Deve ser o nome da coluna da chave primária", tableName, target));
            }


        }


        return result;

    },
    /* *****************************************************************************************************************
     * Valida Colunas
     *
     * @param model
     * ****************************************************************************************************************/
    function (model) {

        var result = {error: [], warn: [], info: []};

        /*



         - INFO: Informa caso o campo default vier entre aspas, exemplo: "'1'" quando deveria ser '1'
         CREATE TABLE t3 (
         ts1 TIMESTAMP NULL DEFAULT 0,
         ts2 TIMESTAMP DEFAULT CURRENT_TIMESTAMP
         ON UPDATE CURRENT_TIMESTAMP);
         TODO: Verificar no SQL oq pode ser feito aqui, se funciona com aspas ou não

         - ERROR: Caso DEFAULT seja um inteiro ou campo numero deve retornar erro TODO: Especifico da linguagem
         - Aqui temos um problema pois no caso do timestamp podemos ter um default complexo:




         */

        for (var tableName in model.tables) {

            var table = model.tables[tableName];
            //console.log(tableName.red);


            /////////////////////////////////////////////
            // Verififica se foi definido Colunas
            /////////////////////////////////////////////
            if (table.columns.length == 0) {
                result.error.push(aUtil.getError("Não foi definido colunas na tabela '%s'!", tableName));

            }

            var hasStatus = false;
            var hasDataCadastro = false;

            for (var i in table.columns) {

                var column = table.columns[i];


                /////////////////////////////////////////////
                // Valida se foi definido tipo
                /////////////////////////////////////////////
                //console.log('-->' + column.title);
                //console.log('---->' + column.type);

                if (!column.type) {
                    result.error.push(aUtil.getError("Não foi definido tipo na coluna '%s' da tabela '%s'!", column.title, tableName));
                }

                /////////////////////////////////////////////
                // Verifica alguns tipos inválidos
                /////////////////////////////////////////////

                // Lista de Tipo inválidos:
                var errorType = ['not'];

                if (errorType.indexOf(column.type.toLowerCase()) !== -1) {

                    result.error.push(aUtil.getError("Não foi definido tipo ou tipo inválido na coluna '%s' da tabela '%s'. Tipo identificado: '%s'!", column.title, tableName, column.type));

                }

                /////////////////////////////////////////////
                // Valida se um campo string foi definido com null
                // Campos do tipo string devem sempre not null, pois podemos usar string vazia,
                // Não devemos definir como obrigatório como not null, pois não funciona!!!!!!
                /////////////////////////////////////////////

                // Mysql
                // TODO: Criar para outras linguagens (verifica a linguagem selecionada, por este código no adapter)
                var stringType = ['char', 'varchar', 'text', 'tinytext', 'mediumtext', 'longtext'];

                if (stringType.indexOf(column.type.toLowerCase()) !== -1){

                    if (column.nullable){
                        result.info.push(aUtil.getInfo("Coluna '%s' da tabela '%s', está definida como string e portanto deve ser '%s'!", column.title, tableName, 'NOT NULL'));
                    }

                }


                /////////////////////////////////////////////
                // Valida se tipo é Válido
                // TODO: Especifico do SGBD, deixar para futura implementação, irá validar ao executar SQL
                /////////////////////////////////////////////

                /////////////////////////////////////////////
                // Valida, dependendo do tipo, se foi especificado o tamanho
                // TODO: Especifico do SGBD, deixar para futura implementação, irá validar ao executar SQL
                /////////////////////////////////////////////

                /////////////////////////////////////////////
                // Valida se está no formato em CamelCase, exceto quando for uma chave (Campo PRIMARY KEY)
                /////////////////////////////////////////////
                var reCamelCase = /^[a-z][a-zA-Z0-9]*[a-z0-9]$/;
                if (!column.title.match(reCamelCase)) {

                    // Se for PRIMARY, significa q é um indice, então tudo bem
                    if (column.type !== 'PRIMARY') {
                        result.warn.push(aUtil.getWarn("Coluna '%s' da tabela '%s' não está no formato camelCase", column.title, tableName));
                    }

                }

                /////////////////////////////////////////////
                // WARN: Valida se Tipo está em maiusculo
                /////////////////////////////////////////////
                var reType = /^[A-Z\s]+$/;

                if (!column.type.match(reType)) {
                    result.warn.push(aUtil.getWarn("Na coluna '%s' da tabela '%s' tipo deve estar em maiúsculo (%s)", column.title, tableName, column.type));
                }

                /////////////////////////////////////////////
                // INFO: Sem comentário
                /////////////////////////////////////////////
                //if (!column.comment) {
                //    result.info.push(aUtil.getInfo("Coluna '%s' da tabela '%s' está sem comentário", column.title, tableName));
                //}

                /////////////////////////////////////////////
                //- INFO: Verifica se campo tem nos ATIVO e sugere mudar para STATUS
                /////////////////////////////////////////////

                if (column.title == 'ativo') {

                    result.info.push(aUtil.getInfo("Na tabela '%s', recomenda-se usar o campo %s em vez de %s, pois assim, podemos ter outros status como 'REMOVIDO'.", tableName, 'status', 'ativo'));

                }

                /////////////////////////////////////////////////////
                // INFO: Verifica se tem campo status ou dataCadastro
                /////////////////////////////////////////////////////
                if (column.title === 'status') {
                    hasStatus = true;
                }
                if (column.title === 'dataCadastro') {
                    hasDataCadastro = true;
                }


                /////////////////////////////////////////////////////
                // ERROR: Verifica se foi definido um PRIMARY para um campo que não é uma chave
                /////////////////////////////////////////////////////

                if (column.type === 'PRIMARY') {

                    var found = false;

                    // Verifica se é chave primária
                    if (aUtil.inArray(column.title, table.primaryKey.columns)) {
                        found = true;
                    }

                    // Verifica se é uma chave estrangeira
                    for (var r in table.relations) {


                        var relation = table.relations[r];

                        if (aUtil.inArray(column.title, relation.foreignKey)) {
                            found = true;
                        }
                    }

                    if (!found) {
                        result.error.push(aUtil.getError("Na tabela %s, coluna %s, tem tipo inválido: %s, só deve ser usado em chave primária ou chave estrangeira", tableName, column.title, "PRIMARY"));

                    }


                }


            }

            /////////////////////////////////////////////////////
            // INFO: Verifica se tem campo status ou dataCadastro
            /////////////////////////////////////////////////////
            if (!hasStatus && tableName.indexOf("__") === -1) {
                result.info.push(aUtil.getInfo("Na tabela '%s', recomenda-se criar o campo %s.", tableName, 'status'));
            }

            if (!hasDataCadastro && tableName.indexOf("__") === -1) {
                result.info.push(aUtil.getInfo("Na tabela '%s', recomenda-se criar o campo %s, com TIMESTAMP e valor default NOW", tableName, 'dataCadastro'));
            }


        }

        return result;

    },
    /* *****************************************************************************************************************
     * Valida Nomes de Tabelas e relacionamento M-N
     *
     * TODO: INFO: indicar possíveis erros no nome, por exemplo tabela que parece ser mas tá com nome errado
     *
     * @param model
     * ****************************************************************************************************************/
    function (model) {

        var result = {error: [], warn: [], info: []};


        var reCamelCase = /^[a-z][a-zA-Z0-9]*[a-z0-9]$/;

        for (var tableName in model.tables) {

            var table = model.tables[tableName];


            ///////////////////////////////////////////////////////
            // WARN: Valida se nome da tabela está em camelCase
            ///////////////////////////////////////////////////////
            if (!tableName.match(reCamelCase)) {


                // Se não está em camelCase significa q deve ser um relacionamento

                if (tableName.indexOf("__") !== -1) {

                    var s = tableName.split("__");
                    var tableA = s[0];
                    var tableB = s[1];

                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Verifica se Tabela A existe
                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (!tableA in model.tables) {
                        result.warn.push(aUtil.getWarn("Tabela %s não existe. Nome da tabela %s deve ser composta pelas tabelas q ela relaciona ", tableA, tableName));
                    }

                    if (!tableB in model.tables) {
                        result.warn.push(aUtil.getWarn("Tabela %s não existe. Nome da tabela %s deve ser composta pelas tabelas q ela relaciona ", tableB, tableName));
                    }

                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Verifica Colunas
                    ////////////////////////////////////////////////////////////////////////////////////////////////////

                    var targetA = tableA + '_id',
                        targetB = tableB + '_id',
                        targetAFound = false,
                        targetBFound = false;


                    for (var c in table.columns) {

                        var column = table.columns[c];

                        if (column.title == targetA) {
                            targetAFound = true;
                        }

                        if (column.title == targetB) {
                            targetBFound = true;
                        }

                    }

                    if (!targetAFound) {
                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter a coluna %s", tableName, targetA));
                    }

                    if (!targetBFound) {
                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter a coluna %s", tableName, targetB));
                    }

                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Verifica INDICE UNICO
                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    var indexTarget = "idx_" + tableA + "__" + tableB;
                    var indexTargetFound = false;

                    for (var idx in table.index) {

                        var index = table.index[idx];

                        if (index.indexName === indexTarget) {
                            indexTargetFound = true;

                            // Verifica as colunas
                            if (!aUtil.inArray(targetA, index.columns)) {
                                result.warn.push(aUtil.getWarn("Indice %s na tabela %s, deve ter a coluna %s", index.indexName, tableName, targetA));
                            }

                            if (!aUtil.inArray(targetB, index.columns)) {
                                result.warn.push(aUtil.getWarn("Indice %s na tabela %s, deve ter a coluna %s", index.indexName, tableName, targetB));
                            }

                            // Verifica se é indice unico
                            if (index.type != "unique") {
                                result.warn.push(aUtil.getWarn("Indice %s na tabela %s, deve se do tipo %s", index.indexName, tableName, "unique"));
                            }

                        }

                    }


                    if (!indexTargetFound) {
                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter o índice único com nome %s", tableName, indexTarget));
                    }


                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Verifica RELACIONAMENTOS
                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    var targetRelationA = 'fk_' + tableA + '___' + tableName,
                        targetRelationB = 'fk_' + tableB + '___' + tableName,
                        targetRelationAFound = false,
                        targetRelationBFound = false;

                    for (var rel in table.relations) {

                        var relation = table.relations[rel];

                        if (relation.foreignKeyName === targetRelationA) {
                            targetRelationAFound = true;

                            if (relation.foreignKey[0] !== targetA) {
                                result.warn.push(aUtil.getWarn("Chave estrangeira %s na tabela %s, foreignKey deve ser %s", targetRelationA, tableName, targetA));
                            }

                            if (relation.referenceKey[0] !== targetA) {
                                result.warn.push(aUtil.getWarn("Chave estrangeira %s na tabela %s, referenceKey deve ser %s", targetRelationA, tableName, targetA));
                            }

                            // Verifica onDelete
                            if (!relation.onDelete || relation.onDelete != 'CASCADE') {
                                result.warn.push(aUtil.getWarn("Chave estrangeira %s na tabela %s, deve ser %s", targetRelationA, tableName, 'ON DELETE CASCADE'));
                            }


                        }

                        if (relation.foreignKeyName === targetRelationB) {
                            targetRelationBFound = true;

                            if (relation.foreignKey[0] !== targetB) {
                                result.warn.push(aUtil.getWarn("Chave estrangeira %s na tabela %s, foreignKey deve ser %s", targetRelationB, tableName, targetB));
                            }

                            if (relation.referenceKey[0] !== targetB) {
                                result.warn.push(aUtil.getWarn("Chave estrangeira %s na tabela %s, referenceKey deve ser %s", targetRelationB, tableName, targetB));
                            }

                            // Verifica onDelete
                            if (!relation.onDelete || relation.onDelete != 'CASCADE') {
                                result.warn.push(aUtil.getWarn("Chave estrangeira %s na tabela %s, deve ser %s", targetRelationB, tableName, 'ON DELETE CASCADE'));
                            }

                        }
                    }

                    if (!targetRelationAFound) {
                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter um relacionamento com nome %s", tableName, targetRelationA));
                    }

                    if (!targetRelationBFound) {
                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter um relacionamento com nome %s", tableName, targetRelationB));
                    }


                }
                else if (tableName.indexOf("_") !== -1) {

                    var s = tableName.split("_");
                    var tableA = s[0];

                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Verifica se Tabela A existe
                    ////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (!tableA in model.tables) {
                        result.warn.push(aUtil.getWarn("Tabela %s não existe. Nome da tabela %s deve ser composta pelas tabelas q ela relaciona ", tableA, tableName));
                    }

                    if (table.relations.length === 0) {
                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter alguma chave estrangeira %s", tableName));
                    }

                    var found = false;
                    for (var rel in table.relations) {
                        var relation = table.relations[rel];

                        if (relation.referenceTable === tableA) {
                            var found = true;
                        }
                    }


                    if (!found) {

                        result.warn.push(aUtil.getWarn("Tabela %s, deve ter relacionamento com a tabela %s", tableName, tableA));

                    }


                }
                else {

                    result.warn.push(aUtil.getWarn("Nome da tabela %s deve estar no formato camelCase", tableName));


                }
            }

        }


        return result;

    },
    /* *****************************************************************************************************************
     * Valida INDICES
     *
     * @param model
     * ****************************************************************************************************************/
    function (model) {

        var result = {error: [], warn: [], info: []};

        var indexs = [];

        for (var tableName in model.tables) {

            var table = model.tables[tableName];


            for (var i in table.index) {

                var index = table.index[i];

                //console.log('--->INDEX: ' + index.indexName);

                /////////////////////////////////////////////////////
                // WARN: Valida se index está com nome correto : idx_[nome_tabela][OPCIONAL]
                /////////////////////////////////////////////////////
                var target = "idx_" + tableName;
                var targetSize = target.length;

                var targetE = "idx_" + tableName + '_';


                if (index.indexName.substring(0, targetSize) !== target) {
                    result.warn.push(aUtil.getWarn("Índice '%s' da tabela '%s' deve começar com %s", index.indexName, tableName, target));
                }

                // Se o nome do indice for mais complexo, deve ter _ separando
                if (index.indexName.length > targetSize && index.indexName.substring(0, targetSize + 1) !== targetE) {
                    result.warn.push(aUtil.getWarn("Índice '%s' da tabela '%s' deve ter um separador '_'", index.indexName, tableName, targetE));

                }

                /////////////////////////////////////////////////////
                // ERROR: Valida se os campos especificado no indice existem
                // parse_dia_sql já faz a validação e não adiciona o campo
                /////////////////////////////////////////////////////

                for (var j in index.columns) {
                    var indexColumn = index.columns[j];

                    var found = false;
                    for (var w in table.columns) {
                        var columns = table.columns[w];
                        if (columns.title == indexColumn) {
                            found = true;
                        }
                    }

                    if (!found) {
                        result.error.push(aUtil.getError("Coluna %s, do índice %s, da tabela %s não foi encontrado!", indexColumn, index.indexName, tableName));
                    }

                }

                /////////////////////////////////////////////////////
                // Valida se o tipo é válido: UNIQUE e (pesquisar os tipos possíveis)
                // validado pelo parse_dia_sql, não carrega se tiver errado
                /////////////////////////////////////////////////////

                // TODO: Atenção, só para MYSQL
                var typesAllow = ['unique', 'fulltext', 'spatial'];

                if (index.type && !aUtil.inArray(index.type, typesAllow)) {


                    result.error.push(aUtil.getError("Índice %s, da tabela %s com tipo inválido, deve ser: %s", index.indexName, tableName, typesAllow.join(', ')));

                }

                /////////////////////////////////////////////////////
                //- ERROR: Verifica se indice já existe
                /////////////////////////////////////////////////////

                if (aUtil.inArray(index.indexName, indexs)) {
                    result.error.push(aUtil.getError("Índice %s, da tabela %s já existe", index.indexName, tableName));
                }

                indexs.push(index.indexName);


            }


        }

        return result;
    },
    /* *****************************************************************************************************************
     * Valida RESTRIÇÕES
     *
     * @param model
     * ****************************************************************************************************************/
    function (model) {

        var result = {error: [], warn: [], info: []};

        var relations = [];

        for (var tableName in model.tables) {

            var table = model.tables[tableName];

            for (var i in table.relations) {

                var relation = table.relations[i];

                ////////////////////////////////////////////////
                // WARN: valida se o nome está no formato fk_[tabela1]_ _ _[tabela2]_[OPCIONAL]
                ////////////////////////////////////////////////

                var targetName = "fk_" + relation.referenceTable + '___' + tableName;
                var targetNameE = "fk_" + relation.referenceTable + '___' + tableName + '_';
                var targetSize = targetName.length;


                if (relation.foreignKeyName.substring(0, targetSize) !== targetName) {
                    result.warn.push(aUtil.getWarn("ForeingKey '%s' da tabela '%s' deve ter o nome iniciado com %s", relation.foreignKeyName, tableName, targetName));
                }


                // Se o nome do indice for mais complexo, deve ter _ separando
                if (relation.foreignKeyName > targetSize && relation.foreignKeyName.substring(0, targetSize + 1) !== targetNameE) {
                    result.warn.push(aUtil.getWarn("ForeingKey '%s' da tabela '%s' deve ter o nome iniciado com %s e com separador '_': ", relation.foreignKeyName, tableName, targetName, targetNameE));

                }


                ////////////////////////////////////////////////
                // ERRO: Valida se referenceTable existe
                ////////////////////////////////////////////////

                var relationTable = null;

                for (var referenceTableName in model.tables) {

                    if (relation.referenceTable === referenceTableName) {
                        relationTable = model.tables[referenceTableName];
                        break;

                    }

                }

                if (!relationTable) {
                    result.error.push(aUtil.getError("ForeignKey %s, da tabela %s, referencia uma tabela que não existe: %s", relation.foreignKeyName, tableName, relation.referenceTable));

                } else {

                    ////////////////////////////////////////////////
                    //- ERRO: Valida se referenceKey existe na tabela remota
                    ////////////////////////////////////////////////

                    var found = false;
                    for (var j in relationTable.columns) {
                        var column = relationTable.columns[j];

                        // TODO: no layout permite mais de uma referencia, mas vamos assumir 1, qualquer coisa alterar
                        if (relation.referenceKey[0] === column.title) {
                            found = true;
                        }
                    }

                    if (!found) {
                        result.error.push(aUtil.getError("ForeignKey %s, da tabela %s, referencia uma coluna na tabela %s que não existe: %s", relation.foreignKeyName, tableName, relation.referenceTable, relation.referenceKey[0]));
                    }

                    ////////////////////////////////////////////////
                    // ERRO: Valida se foreignKey existe localmente
                    ////////////////////////////////////////////////

                    var found = false;


                    for (var j in table.columns) {
                        var column = table.columns[j];


                        // TODO: no layout permite mais de uma referencia, mas vamos assumir 1, qualquer coisa alterar
                        if (relation.foreignKey[0] === column.title) {
                            found = true;

                            ///////////////////////////////////////////////
                            // Valida se é do tipo primary
                            ///////////////////////////////////////////////
                            if (column.type !== "PRIMARY") {
                                result.warn.push(aUtil.getWarn("Coluna %s da tabela '%s' deve ser do tipo %s e não (%s), pois pertence a chave estrangeira ", column.title, tableName, 'PRIMARY', column.type, relation.foreignKeyName));

                            }
                        }
                    }

                    if (!found) {
                        result.error.push(aUtil.getError("ForeignKey %s, da tabela %s, referencia uma coluna que não existe: %s", relation.foreignKeyName, tableName, relation.foreignKey[0]));
                    }

                }

                /////////////////////////////////////////////////////
                //ERROR: Verifica se restrição já existe
                /////////////////////////////////////////////////////

                if (aUtil.inArray(relation.foreignKeyName, relations)) {
                    result.error.push(aUtil.getError("ForeignKey %s, da tabela %s já existe", relation.foreignKeyName, tableName));
                }

                relations.push(relation.foreignKeyName);

            }
        }


        return result;

    }


];

var aUtil = {


    /**
     * Gera uma mensagem colorida, baseado nos argumentos
     * TODO: Por no autil
     */
    renderMsg: function () {

        var msg = [];

        for (var i in arguments) {
            if (i == 0) {
                msg.push(arguments[i]);
            } else {
                msg.push(arguments[i].bold.blue);
            }
        }

        return util.format.apply(this, msg);


    },

    /**
     * Imprime um AVISO
     * TODO: Por no autil, criar warn
     */
    getWarn: function () {
        return "WARN: ".red + this.renderMsg.apply(this, arguments);
    },

    /**
     * IMPRIME ERRO
     * TODO: Por no autil, , criar error
     */
    getError: function () {
        return "ERROR: ".red.bold + this.renderMsg.apply(this, arguments);
    },

    /**
     * IMPRIME ERRO
     * TODO: Por no autil, criar info
     */
    getInfo: function () {
        return "INFO: ".green.bold + this.renderMsg.apply(this, arguments);
    },

    /**
     * Verifica se um valor está no array
     *
     * TODO: por no util
     * @param item
     * @param array
     */
    inArray: function (item, array) {

        for (var i in array) {
            if (array[i] == item) {
                return true;
            }
        }

        return false;

    }

};