/**
 * Created by andre on 24/11/14.
 *
 * Terá várias ferramentas para trabalhar com Modelo e dados, como importação e exportação
 *
 *
 *
 */

var _ = require('lodash'),
    _s = require("underscore.string"),
    util = require('util');

require('colors');

module.exports = {


    /**
     * Conexão com o Banco de dados
     */
    connection: null,

    /**
     * Cliente de Conexão com o banco de dados
     */
    client: 'mysql',

    /**
     * Instancia do Knex
     */
    knex: null,

    /**
     * Lista de Querys para ser executada
     */
    querys: [],

    /**
     * Verboso
     */
    verbose: false,

    /**
     * Cria um novo banco de dados, baseado em uma estrutura (model) json
     *
     * Através de um parâmetro é possível criar SQL em vez de Salvar
     *
     * @param model
     * @param drop          Limpa Base de dados, remove e recria. Não é possível criar um banco de dados novo.
     * @param callback      Retorno quando operação concluída, true p/ sucesos fals e/erro
     */
    createDb: function (model, drop, callback) {

        var self = this;

        if (!model instanceof Object) {

            throw new Error("Modelo Inválido, deve ser um objeto");

        }

        // Continua execução do script
        var createDb2 = function () {

            self._initKnex();

            // Monta Query Para Criação de Tabela
            self.querys = self.querys.concat(self._createTables(model));

            // Monta QUery Para Criação de Refêrencias
            self.querys = self.querys.concat(self._createReferences(model));

            // Monta QUery Para Criação de Refêrencias
            self.executeQuerys(function (status) {

                if (status) {
                    console.log("\nQuery executada com sucesso.");
                } else {
                    console.log("\nQuery executada com erro.");
                }

                callback(status);

            });


        };


        // Inicializa KNEX
        self._initKnex();

        if (drop && self.connection) {

            self.querys.push(self.knex.raw(util.format('DROP DATABASE `%s`', self.connection.database)));
            self.querys.push(self.knex.raw(util.format('CREATE DATABASE `%s`', self.connection.database)));

            self.executeQuerys(function (status) {

                if (status) {
                    createDb2();
                }


            });

        } else {

            createDb2();

        }


    },


    /**
     * Executa Query adicionada no lista de query
     *
     * @param callback  Executa quando tudo tiver pronto, parametro true se tudo cert, false se errado
     */
    executeQuerys: function (callback) {

        var self = this;

        if (self.connection) {

            self._executeQuerys(callback).catch(function (error, x) {

                //console.log("_____________________ ERROR ___________________________");
                self.knex.destroy();
                console.log(error.name.red.bold);
                console.log(error.message.red);
                console.log('Code: '.bold + error.code);
                console.log('Errno: '.bold + error.errno);
                console.log('Sql State: '.bold + error.sqlState);
                console.log('Index: '.bold + error.index);


                if (callback) callback(false);


            });

        } else {

            self._executeQuerys();
        }


    }
    ,

    /**
     * Função de Auxilio
     */
    _executeQuerys: function (callback) {

        var self = this;
        var query = self.querys.shift();


        //////////////////////////////////////////////////////////
        // SE FOR EXECUTADO DIRETO NO DB
        //////////////////////////////////////////////////////////
        if (self.connection) {

            if (query) {

                if (self.verbose) {
                    console.log(query.toString() + "\n");
                } else {
                    util.print(".");
                }


                return query
                    .then(function (data) {

                        //console.log("_____________________ NEXT ___________________________");
                        return self._executeQuerys(callback);

                    });


            } else {

                //console.log("_____________________ END ___________________________");
                self.knex.destroy();

                if (callback) callback(true);

            }

        } else {

            if (query) {
                console.log(query.toString(), ";\n");
                self._executeQuerys();
            }

        }


    }
    ,


    /**
     * Importa dados salvos no formato JSON para o Banco de dados
     *
     * Através de um parâmetro é possível criar SQL em vez de Salvar
     *
     */
    importData: function () {

    }
    ,

    /**
     * Exporta dados do Banco de dados para um arquivo no formato JSON
     *
     * Através de um parâmetro é possível criar SQL em vez de Salvar
     *
     */
    exportData: function () {

    }
    ,

    /**
     * Analise 2 modelos diferentes, e verifica o que falta para carregar dados de um modelo em outro, criando gabaritos
     * para preencher com as diferenças
     *
     * @param modelFrom
     * @param modelTo
     */
    modelAnalysis: function (modelFrom, modelTo) {


        /**
         * Sempre comparando o modelTo com modelFrom
         * ================================================
         * ANALISA TABELA
         * ================================================
         *
         * Possíveis casos:
         * - Já existe
         * - Nova
         * - Deriva de Outra
         * - Deriva de Outras
         *
         * Primeiro faz Análise automatica, podento ter 2 resultados:
         * - Já existe
         * - Nova
         *
         * Será exibido uma tela onde será possível alterar
         *
         * ex:
         *
         * Tabela  |   Origem         |
         * --------|------------------|
         * tabelaA | tabelaA          |
         * tabelaB | tabelaC          |
         * TabelaD | tabelaE, TabelaF |
         * TabelaX | NOVA             |
         * --------|------------------|
         *
         *
         * Será criado um link entre as tabelas, que será usado no futuro para copiar os valores entre as colunas
         *
         * ================================================
         * ANALISA REFÊRENCIA DAS COLUNAS
         * ================================================
         *
         * - Para cada coluna tenta identificar de onde vem no schema antigo
         * - Deverá ser feito um link da coluna com a coluna do schema antigo, por padrão é o mesmo, mas pode variar
         * - Se a tabela tiver 1 origem:
         *      - Verifica se existe na origem, se sim LINKA
         *      - Se não, MARCA COMO NOVO
         * - Se a tabela tiver 2 ou mais origens
         *      - Se existir em 1 delas, LINKA
         *      - Se existir em 2 delas, PERGUNTA qual será usada
         *      - Se não existir em nenhuma delas, MARCA COMO NOVO
         *
         *
         * ================================================
         * ANALISA REGRA PARA CÒPIA DE DADOS
         * ================================================
         *
         * Depois da referencia completa, será tentado fazer a cópia de dados na seguinte ordem:
         * (Caso não seja possíve, vai pra próxima regra)
         *
         * 1- COPIA DIRETA   - Copia normalmente
         * 2- CONVERTE       - Caso tenha mudado o tipo e seja possível a conversão (ex: INT -> STRING)
         * 3- TRUNCA         - Trunca os dados, (ex: se o tamanho diminuiu, joga fora o valor restante)
         * 4- DEFAULT        - Define o valor default (Se campo não tiver defult, vai pra próxima)
         * 5- SET NULL       - Seta null, (Se o campo for not null, vai pra próxima)
         * 6- PROMPT DEFAULT - Abre um prompt e pergunta qual o valor será usado como default (se for unique, vai pra próxima)
         * 7- FUNÇÃO         - Abre um prompt perguntando se deseja definir uma função para gerar os valores
         *                     (TODO: DEFINIR) (SERÁ VALIDADO NA HORA DE REALIZAR MIGRAÇÃO)
         * 8- GABARITO       - Ultimo caso se gera um gabarito no formato CSV com todos os valores atuais, deve-se
         *                     defifinir ao lado os novos valores. (SERÁ VALIDADO NA HORA DE REALIZAR MIGRAÇÃO)
         *                     Ex: campo CPF
         *
         * Será criado uma tela onde será possível reconfigurar, exemplo:
         *
         *
         * Tabela  | Coluna  | Origem          | Ação         |
         * --------|---------|-----------------|--------------|
         * Tabela1 | Coluna1 | Tabela1.coluna1 | COPIA DIRETA |
         * Tabela1 | Coluna2 | Tabela1.coluna3 | SET NULL     |
         * Tabela2 | ColunaX | Tabela2.colunaX | GABARITO     |
         *
         * ================================================
         * ANALISE DE DADOS
         * ================================================
         *
         * Será feito uma análise de dados para identeificar as acções possíveis,
         *
         * Ex: Um campo mudou e agora é unico.
         *      Podemos copiar do esquema antigo, DESDE QUE NÃO SE REPITA. Se não se repetir, disponibiliza a opção
         *      COPIA DIRETA, se não não
         *
         *      TODO: Criar uma relação entre as diferentes mudanças:
         *      Ex: DEFAULT, UNIQUE, NOT NULL etc...
         *
         * ================================================
         * RELACIONAMENTO
         * ================================================
         *
         * Chaves estrangeiras tem um tratamento especial
         *
         * -------------------------------
         * Deixou de ser chave estrangeira
         * -------------------------------
         *
         * Teremos a ação:
         *  1 - (RELACIONAMENT0) COPIAR VALOR DO DESTINO, e poderemos definir qual o campo
         *
         *  Ou seja se tabelaA tem o campo tabelab_id, podemos dizer que o campo tabelab_id vai receber o valor da coluna
         *  no registro no qual a tabelab_id aponta
         *
         * Apartir dae, se aplica as 8 regras definidas acima
         *
         *
         * 2- (DIRETO) Podemos LINKAR como campo normal, e definir outro campo para copiar os dados
         *
         * O relatório então na verdade ficará assim:
         *
         *
         * Tabela  | Coluna  | Origem          | Ação         | Link
         * --------|---------|-----------------|--------------|----------------
         * Tabela1 | Coluna1 | Tabela1.coluna1 | COPIA DIRETA | DIRETO
         * Tabela1 | Coluna2 | Tabela1.coluna3 | SET NULL     | RELACIONAMENTO
         * Tabela2 | ColunaX | Tabela2.colunaX | GABARITO     | DIRETO
         *
         * NOTE: Oq vai definir se pode ou não ser relacionamento é o fato do campo ser chave estrangeira na ORIGEM
         *
         * ---------------------------------
         * Passa a ser uma chave estrangeira
         * ---------------------------------
         *
         * Aplica as 8 regras da cópia de dados. Note como é campo novo efetivamente, só vale apartir do item 4
         *
         * Porém aqui podemos ter um bloqueio de importação:
         *
         * Ex:  Se o novo campo for UNIQUE e NOT NULL e não existir link suficiente (ex: 1 para 1 obrigatório)
         * Nem gabarito seria suficiente
         *
         * ---------------------------------
         * Análise de Destino
         * ---------------------------------
         *
         * Pode ocorrer do destino mudar, casos possíveis:
         * * Tabela destino foi renomeada (ok, só puxar da outra tabela)
         * * Coluna destino foi renomeada (ok, só pudar a outra coluna)
         *
         * Usar a biblioteca SBoudrias/Inquirer.js
         *
         *
         */


    }
    ,

    /**
     * Cria tabela a partir do Modelo
     *
     *
     * @param model
     * @private
     */
    _createTables: function (model) {

        var self = this;

        var querys = [];

        _.forEach(model.tables, function (table, tableName) {

            querys.push(self._createTable(table, tableName));

        });

        return querys;

    }
    ,

    /**
     * Cria Tabela
     * @param table
     * @param tableName
     * @private
     */
    _createTable: function (table, tableName) {

        var self = this;

        // Aqui fiz uso do  function.bind: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
        // adicionei mais 2 parâmetros, table e tableName, além claro de passa o escopo (obrigatório sempre)
        return self.knex.schema.createTable(tableName, function (t) {

            /////////////////////////////////////////////////////////
            // Cria Coluna
            /////////////////////////////////////////////////////////
            self._createColumn(table, tableName, t);

            /////////////////////////////////////////////////////////
            // Configura Chave Primária
            /////////////////////////////////////////////////////////

            // TODO: ATENÇÃO: Não é possível definir uma chave primaria auto increment separadamente necessário criar por fora, então um dia para salvar mais de uma chave, teremos q alterar aqui
            //t.primary(table.primaryKey.columns);

            /////////////////////////////////////////////////////////
            // Configura Indices
            /////////////////////////////////////////////////////////

            self.__crateIndex(table, tableName, t);


        });


    }
    ,

    /**
     * Configura COluna
     *
     * @param table
     * @private
     */
    _createColumn: function (table, tableName, t) {

        var self = this;

        _.forEach(table.columns, function (column) {

            var newColumn;

            ////////////////////////////////////////////////////////////////
            // TIPO ESPECIAL PRIMARY
            ////////////////////////////////////////////////////////////////
            if (column.type === 'PRIMARY') {

                // TODO: Por padrão chave primária é auto increment, obrigatóriamente, mas pode existir casos que não será, tratar, criando um novo tipo como PRIMARY_AUTOINCRMENT ou APRIMARY

                if (table.primaryKey.columns.indexOf(column.title) !== -1) {

                    newColumn = t.increments(column.title);

                } else {

                    // Chave Estrangeira

                    newColumn = t.specificType(column.title, 'int unsigned');

                }


            }

            ////////////////////////////////////////////////////////////////
            // SE COLUNA TIVER TAMANHO
            ////////////////////////////////////////////////////////////////
            // Note que vamos criar o tipo arbitráriamente, se foi criado errado no modelo, vai falhar aqui
            else if (column.typeSize) {


                newColumn = t.specificType(column.title, util.format("%s (%s)", column.type, column.typeSize));


            }

            ////////////////////////////////////////////////////////////////
            // SE COLUNA NÃO TIVER TANANHO
            ////////////////////////////////////////////////////////////////
            else {

                newColumn = t.specificType(column.title, column.type);


            }

            ////////////////////////////////////////////////////////////////
            // NULLABLE
            ////////////////////////////////////////////////////////////////

            if (column.nullable) {
                newColumn.nullable();
            } else {
                newColumn.notNullable();
            }

            ////////////////////////////////////////////////////////////////
            // VALOR DEFAULT
            ////////////////////////////////////////////////////////////////
            // TODO: Existe os casos especiais, tratar
            // TODO: Testar com a geração do dia, pois tem gerado com aspas
            if (column.default) {

                newColumn.defaultTo(self.knex.raw(column.default));

            }


            ////////////////////////////////////////////////////////////////
            // COMENTARIO
            ////////////////////////////////////////////////////////////////
            if (column.comment) {
                newColumn.comment(column.comment)
            }


        });


    }
    ,

    /**
     * Cria Indices
     *
     * @param table
     * @param tableName
     * @param t table (instancia de coluna do knex)
     * @private
     */
    __crateIndex: function (table, tableName, t) {

        var self = this;

        _.forEach(table.index, function (index) {

            if (index.type === 'unique') {

                t.unique(index.columns, index.indexName);


            } else {

                //TODO: Quando for gerar um indice verificar a forma correta, na documentação diz de uma forma, no fonte outro, testar
                // Problema está no indexName, se vc passar errado ele vai gerar um nome muito grande q vai estourar o limite do mysql
                //t.index(index.columns, index.type);
                t.index(index.columns, index.indexName);

                throw new Error("Verificar a forma correta");
            }

        });


    }
    ,

    /**
     * Gera Refêrencias
     *
     * Devido a uma limitação do Knex que não permite adicionar chave estrangeira sem recriar a coluna,
     * tive que fazer este "workAround",
     * TODO: verificar se já foi atualizado: https://github.com/tgriesser/knex/issues/558
     * TODO: Funciona para mysql, outros sgbds será necessário implementar
     * TODO: Colocar este código no adapter correspondente
     *
     *
     * Precisamos criar os releacionamento depois de gerar todas as tabelas. Porém o Knex dá duas opções:
     *  - Criar o relacionamento emquanto cria a coluna (Quebra, pois a tabela referenciada pode não ter sido criado ainda)
     *
     *  - Alterar a tabela: Porém o knex tenta criar a coluna denovo (não sei pq, é um bug).
     *
     *  Vamos então usar o knex para criar o SQL de alteração de tabela, mas depois de gerado vamos cortar o fora o trecho
     *  que tenta recriar a Coluna, para isso vamos usar uma expressão regular
     *
     *  Aproveitando, o knex não suporta o nome personalizado de relacionamento, vamos adicionar este suporte (acompanhe)
     *
     *  Podemos usar o Knex para gerar as consultas e executa-las imediatamente, mas neste caso vamos fazer diferente:
     *  - Mandamos o Knex gerar as consultas
     *  - Em vez de executar, vamos pegar essas consultas
     *  - Processa-las
     *  - E adicionar no KNEX denovo como consultas RAW
     *
     * @param model
     * @private
     */
    _createReferences: function (model) {

        var self = this;

        var querys = [];


        _.forEach(model.tables, function (table, tableName) {


            /////////////////////////////////////////////////////////////////////////////////////
            // Vamos Iniciar o processo de alteração de tabela
            /////////////////////////////////////////////////////////////////////////////////////

            // Vamos dizer oa knex que queremos aletar uma nova tabela já existente (veja q estamos usando um schema pronto)
            var alterTable = self.knex.schema.table(tableName, function (t) {

                // Vamos Percorrer todos os relacionamentos do nosso modelo(vindo dos parãmetros)
                _.forEach(table.relations, function (relation) {

                    // Vamos dizer ao knex que queremos criar uma nova coluna (lembre q o knex não permite alterar o
                    // relacionamento sem estar criando uma nova coluna)
                    var alterColumn = t
                        .specificType(relation.foreignKey[0], 'int unsigned')
                        // Já aproveitamos e configuramos o relacionamento
                        .references(relation.referenceKey[0])
                        // Note q o Knex não permite dizer o nome do relacionamento, então vou  injetar o nome depois
                        // do nome ta tabela referenciada, envolta de "|" vamos usar esse simbolo para extrair depois
                        // com regex
                        .inTable(relation.referenceTable + '|' + relation.foreignKeyName + '|');


                    // COnfiguramos ondelete nesse relacionamento
                    if (relation.onDelete) {
                        alterColumn.onDelete(relation.onDelete)
                    }

                    // COnfiguramos ondelete nesse relacionamento
                    if (relation.onUpdate) {
                        alterColumn.onUpdate(relation.onUpdate)
                    }

                })


            });

            /////////////////////////////////////////////////////////////////////////////////////
            // Gera RELACIONAMENTO
            /////////////////////////////////////////////////////////////////////////////////////

            // Aqui é o pulo do gato, vamos percorrer todas as linhas geradas pelo knex (toString), e vamos descrtar as
            // linhas que cria a coluna (vamos ficar apenas com as consultas q iniciam com alter table)

            // Aproveitando vamos dividir a consulta em 7 pedaços:
            // Pedaço 1: Inicio da Consulta
            // Pedaço 2: Nome do relacionamento que o knex gerou que não queremos (vamos jogar fora esse pedaço)
            // Pedaço 3: Parte do meio da consulta
            // Pedaço 4: O nosso curinga, que vamos usar para a expressão regular encontrar o nome desejado (vamos jogar fora este pedaço)
            // Pedaço 5: O nome que queremos para o relacionamento
            // Pedaço 6: O outro curinga q delimita o nome desejado (vamos jogar fora este pedaço)
            // Pedaço 7: O restante da consulta

            // Agora vamos juntar as partes: Pedaços: 1(inicio) + 5(nome desejado) + 3(meio) + 7(fim)
            var reWorkAround = /^(alter table `\w+` add constraint )(\w+)( foreign key.*)(\|)([\w]+)(\|)(.*)$/;

            _.forEach(alterTable.toString().split('\n'), function (line) {

                var match = line.match(reWorkAround);

                if (match) {

                    var newQuery = match[1] + match[5] + match[3] + match[7];
                    querys.push(self.knex.raw(newQuery));
                }
            });

        });

        return querys;

    }
    ,

    /**
     *  Inicia instancia do KNEX
     *
     * @private
     */
    _initKnex: function () {

        var self = this;


        if (self.connection) {
            self.knex = require('knex')({
                client: self.client,
                connection: self.connection
            });
        } else {
            self.knex = require('knex')({
                client: self.client
            });
        }


    }

}
;