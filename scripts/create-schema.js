/**
 * Created by andre on 23/11/14.
 *
 */
"use strict";

const SindriCli = require('sindri-cli');
const Migrate = require('../lib/migrate');

class CreateSchema extends SindriCli {
    execute(argv) {

        Migrate.configByArgv(argv);

        Migrate.createSchema(
            argv.file,
            argv.nputFormat ? argv.inputFormat : 'dia',
            argv.diaOutputFormat ? argv.diaOutputFormat : 'mysql-innodb',
            argv.client ? argv.client : "mysql",
            argv.loadInvalid,
            argv.save
        );

    }


    getShortInfo() {

        return "Cria um novo esquema baseado com a ultima versão do modelo de banco de dados.";

    }


    help() {

        return `Cria um novo esquema (schema) na pasta de schema. O nome será representado por um diretório no formato:\n
\tschema-[yyyymmdd]-[00000]".cyan + "\n
Onde, 00000 é um numero sequencial de 5 dígitos. Dentro será criado o arquivo:\n
\tmigration-schema.json".cyan + "\n
\nTerá também configurações sobre a migração, a definir".white`;

    }


    args() {

        return {

            '--file': 'Arquivo de origem, diagrama que será carregado para gerar o esquema, deve estar no formato padrão do DIA. Caso nenhum arquivo seja passado, irá carregar a ultima versão dentro do diretório \'source\'',
            '--path': 'Localização da estrutura de migração. Padrão: Diretório atual',
            '--dia-output-format': 'Formato do arquivo gerado pelo parser do DIA. Padrão: "mysql-innodb". Opções: (db2, html, informix, ingres, innodb, mssql, mysql-innodb, mysql-myisam, oracle, postgres, sas, sqlite3, sqlite3fk, sybase)',
            '--client': 'Cliente de banco de dados. Padrão: mysql (único disponível). Nota: Caso use o DIA, deve ser compatível com o formato especificado por --dia-output-format. Atualmente apenas compatível com mysql-innodb.',
            '--input-format': 'Formato do arquivo de entrada, dia ou sql (por enquanto apenas dia disponível)',
            '--loadinvalid': 'Tenta gerar schema, mesmo não válido',
            '--save': 'Salva schema, sem esta opção, apenas valida arquivo'


        };
    }


}

module.exports = CreateSchema;



