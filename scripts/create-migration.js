/**
 * Created by andre on 23/11/14.
 *
 * // TODO: Caso seja decidido salvar diretamente no banco de dados, testar conexão antes de criar migração
 * // TODO: Permitir salvar os dados de conexão para não precisar digitar nas próximas vezes
 * // TODO: Criptografar senha para melhorar um pouco a segurança
 *
 */
"use strict";

const SindriCli = require('sindri-cli');
const Migrate = require('../lib/migrate');
const _ = require('lodash');

require('colors');

class CreateMigration extends SindriCli  {

    execute(argv) {


        var options = this.configureArgs(argv);

        options
            .c('forceNew', 'new')
            .c('saveSql')
            .c('backup')
            .c('drop')
            .c('client')
            .c('host', ['h', 'host'])
            .c('user', ['u', 'user'])
            .c('password', ['p', 'password'])
            .c('database', ['d', 'database'])
            .c('clientOrigin')
            .c('hostOrigin')
            .c('userOrigin')
            .c('passwordOrigin')
            .c('databaseOsrigin');


        Migrate.configByArgv(argv);
        Migrate.createMigration(
            argv._[1] || argv['schema-target'],
            options
        );


    }

    help() {

        return "Prepara uma nova migração.\nAnálisa as diferenças entre os esquemas que serão migrados, realiza validações, gera gabaritos pra preenchimento com dados.\n" +
            "Em seguida deve-se preencher os gabaritos com informações usadas na migração e por fim executar o run-migration.";


    }


    getShortInfo() {

        return "Prepara uma nova migração. Caso queira salvar diretamente na base de dados, necessário configurar conexão na primeira vez."

    }


    args() {

        return {
            '--schema-target, [arg1]': 'Id do esquema para onde a base de dados será migrada, se não for especificado, será criado uma migração para o esquema mais recente.',
            '--new': 'Força criação de uma nova base de dados.' + ' ATENÇÃO: Os dados atuais na base serão perdidos. Será criado uma nova base de dados limpa.'.bold.red,
            '--path': 'Localização da estrutura de migração. Padrão: Diretório atual',
            '--save-sql': 'Gera um arquivo SQL em vez de trabalhar com a base de dados diretamente',
            '--no-backup': 'Não realiza backup dos dados',
            '--drop': 'Remove e cria base de dados caso não exista',
            '--client': 'Cliente da base de dados. Padrão: \'mysql\'',
            '-h, --host': 'Host do base de dados',
            '-u, --user': 'Usuário da base de dados',
            '-p, --password': 'Senha da base de dados',
            '-d, --database': 'Nome da base de dados (database ou schema)',
            '--client-origin': 'Usado quando realizado migração entre diferentes bases de dados, se null usa o mesmo de client, ou então especificar base de origem',
            '--host-origin': 'Host do base de dados de origem',
            '--user-origin': 'Usuário da base de dados de origem',
            '--password-origin': 'Senha da base de dados de origem',
            '--database-origin': 'Nome da base de dados (database ou schema) de origem'
        };

    }


}

module.exports = CreateMigration;


