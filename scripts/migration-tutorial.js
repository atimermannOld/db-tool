/**
 * migration-tutorial
 * Created by andre on 27/02/15.
 */
"use strict";

const SindriCli = require('sindri-cli');
require('colors');

class MigrationTutorial extends SindriCli {

    execute(argv) {

        var t;

        t = "\n\n----------------------------------\nEtapa 01: Gerendo estrutura\n----------------------------------\n".bold;
        t += "\tUse o comando:";
        t += " sindri init-migration ".yellow.bold;
        t += " para gerar a estrutura de diretório básica para criar migrations.";

        t += "\n\n------------------------------------\nEtapa 02: Modelagem de Dados\n----------------------------------\n".bold;
        t += "\tPrimeira etapa é modelar a base de dados, por enquanto a unica ferramenta disponível é DIA na dialéto MYSQL. O Arquivo deve ser salvo no formato .dia e armazenado no diretório:";
        t += " data/source/ ".yellow.bold;
        t += " e no formato ";
        t += " consultorio_legal-NNNNN.dia ".yellow.bold;
        t += " , onde NNNNN é um numero sequencial. O sistema sempre carregará o arquivo cujo numero sequencia seja maior. DICA: Pode ser um link simbólico para outro arquivo."


        t += "\n\n----------------------------------\nEtapa 03: Criação de Schema\n----------------------------------\n".bold;
        t += "\tUse o comando:";
        t += " sindri create-schema ".yellow.bold;
        t += " enquanto está modelando o base de dados parar validar o modelo. Depois de pronto execute o mesmo comando com o parâmetro:";
        t += " --save".yellow.bold
        t += " para criar o arquivo de schema. Que ficará salvo em";
        t += " data/schema/".yellow.bold;
        t += " DICA: Veja as outras opções disponívels do comando";

        t += "\n\n----------------------------------\nEtapa 04: Preparação da Migração.\n----------------------------------\n".bold;
        t += "\tEstá é a etapa mais importante e delicada, aqui iremos definir todas as configurações para criar nosso arquivo de migração. IMPORTANTE: aqui definimos toda configuração da migração, na proxima etapa executaremos a migração.\n";
        t += "\n\tExecute ";
        t += "sindri help create-migration ".yellow.bold;
        t += "para ver todas as opções. Basicamente você precisa configurar como será feita a migração:\n";
        t += "\t- Definir as configurações da base de dados onde será feita a migração (sgbd, hostname, database, usuário e senha.\n";
        t += "\t- Caso a migração seja feita entre dois bando de dados diferente, necessário configurar a base de origem (mesmos parâmetros descrito acima).\n";
        t += "\t- Se vai fazer uma migração ou gerar uma nova base de dados (--new).\n";
        t += "\t- Ser irá realizar backup da base antiga (--no-backup).\n";
        t += "\t- Se a base será removida e criada automaticamente (--drop).\n";
        t += "\n\nExemplo:\n";
        t += "\tsindri  create-migration -h localhost -u root -p minha_senha -d meu_database --new --drop\n".yellow.bold;
        t += "\n\nNOTA: ".bold;
        t += "Caso você crie uma migração inválida, terá que apagar manualmente o arquivo de migração do diretório ";
        t += " data/migration ".yellow.bold;
        t += " e só poderá criar outra migração quando processar a última criada.\n";
        t += "Ao criar uma migração, será gerado um gabarito de valores (Não implementado), onde vamos definir as regras de conversão de dados da migração.";

        t += "\n\n----------------------------------\nEtapa 05: Executar Migração.\n----------------------------------\n".bold;

        t += "\tEtapa final, nenhuma configuração será feita aqui, apenas realização da migração. Execute: ";
        t += " sindri run-migration --verbose".yellow.bold;


        console.log(t);


    }


}

module.exports = MigrationTutorial;
