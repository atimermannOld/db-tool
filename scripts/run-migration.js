/**
 * Created by andre on 23/11/14.
 *
 * // TODO: Permitir especificar banco de dados aqui
 */
"use strict";

const SindriCli = require('sindri-cli');
const Migrate = require('../lib/migrate');

class RunMigration extends SindriCli {

    execute(argv) {

        Migrate.configByArgv(argv);
        Migrate.runMigration(argv.v);


    }


    help() {


        return "Executa migração. Banco de dados será removido e recriado com a nova versão. Caso não seja uma nova migração, então os dados serão migrados da versão antiga\n" +
            "\tSerá realizado backup dos dados antes de migração";

    }

    getShortInfo() {

        return "Executa migração";

    }

    args() {

        return {
            '--path': 'Localização da estrutura de migração. Padrão: Diretório atual',
            '-v': 'Verboso'
        }


    }

}

module.exports = RunMigration;

