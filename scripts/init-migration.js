/**
 * Created by andre on 23/11/14.
 *
 * Prepara ambiente de Migração
 *
 */
"use strict";

const SindriCli = require('sindri-cli');
const Migrate = require('../lib/migrate');

class InitMigration extends SindriCli {

    execute(argv) {


        Migrate.configByArgv(argv);
        Migrate.init();

    }


    getShortInfo() {

        return "Cria uma nova estrutura de diretório para a ferramenta de migração. Por padrão 'data'";
    }


    help() {

        return this.getShortInfo();
    }


    args() {

        return {
            '--path': 'Local onde será criado a estrutura inicial, padrão: DIRETÓRIO ATUAL'

        };

    }

}

module.exports = InitMigration;
