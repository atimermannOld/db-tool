/**
 * Created by andre on 23/11/14.
 */
"use strict";

const SindriCli = require('sindri-cli');
const convertTool = require('../lib/convert-tool');

class Model2Sql extends SindriCli {

    execute(argv) {
        convertTool.modeljson2sql(argv.file);
    }

}

module.exports = Model2Sql;
