#!/usr/bin/env nodejs
/**
 * Created by andre on 23/11/14.
 */

var cli = require('cli-base').cli,
    util = require('util'),
    path = require('path');

require('colors');

cli.scriptDir = path.resolve(__dirname, 'scripts');

cli.help = function () {


    var help =
        "Para obter ajuda de um comando em particular digite:\n" +
        "db-tool help ".red + "<command-name>".red.bold;

    util.print(help + "\n");


};

cli.load(__dirname);