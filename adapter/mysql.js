/**
 * Created by André Timermann on 31/10/14.
 *
 * PARSER MYSQL
 *
 * Processa Arquivo MySQL
 *
 * RETORNA JSON
 *
 * TODO: Para a versão gerada pelo parsediaql: Para SQL personalizado precisa melhorar, Marcação: TODO: [PARSEDIASQL]
 *
 */

var adapterBase = require('../lib/adapterBase.js');

var mysql;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PARSER: CREATE TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var parserCreateTable = {
    "matchIn": function (line) {
        "use strict";

        //console.log("MATCH IN|" + line);

        var re = /^create table \w* \(/;

        return line.match(re);


    },

    "matchOut": function (line) {
        "use strict";

        //console.log("MATCH OUT: " + line);
        var re = /^\)\s*ENGINE=InnoDB DEFAULT CHARSET=latin1;/;

        return line.match(re);

    },

    "parser": function (buffer) {
        "use strict";

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Carrega o nome da tabela (deve estar na primeira linha)
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        var tableNameRegResult = buffer[0].match(/^create table (\w*) \(/);
        var tableName = tableNameRegResult[1];

        /* Reg Exp para extrair informação da coluna:
         Explicação:
         Ex: ativo          BIT (1)        default \'1\' NOT NULL               ,--  0 -> False 1 -> True',

         /^
         \s+
         (\w+) > Pega o nome da Coluna (obrigatório)
         \s+
         (\w+) > Pega o TIPO DO CAMPO (obrigatório)
         \s?
         (\(\d+\))? > Pega o Tamanho do campo, note que é opcional (?)
         \s+
         (default ([\w\\']+)\s+)? > Pega o valor default (Opcional), note queremos o valor
         (not)?  > Se é NOT NULL (aqui só pega o NOT) (OPCIONAL)
         \s?
         (null)? (OPCIONAL)
         \s*
         ,
         (--\s*(.*))? > Pegamos o comentário, opcional também
         /i
         */
        var reColumn = /^\s+(\w+)\s+(\w*(?:\sunsigned)?(?:\szerofill)?)\s?(?:\((\d+)\))?\s+(?:default ([\w\\']+)\s+)?(not)?\s?(?:null)?\s*,?(?:--\s*(.*))?$/i;

        var rePk = /\s+constraint (\w+) primary key \((\w+)\)/i;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Carrega uma tabela se não existir cria
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        var table = mysql.table(tableName);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Percorre cada Linha
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        buffer.forEach(function (line) {

            /////////////////////////////////////////
            // EXTRAI COLUNA
            /////////////////////////////////////////
            var column = line.match(reColumn);

            if (column) {
                table.addColumn({
                    title: column[1],
                    type: column[2],
                    typeSize: column[3],
                    default: column[4],
                    nullable: column[5] ? false : true,
                    comment: column[6]
                });
            }

            /////////////////////////////////////////
            // PRIMARY KEY
            /////////////////////////////////////////

            var pk = line.match(rePk);

            if (pk) {

                table.setPrimaryKey({
                    primaryKeyName: pk[1],
                    columns: pk[2].split(',')
                });

            }


        });


    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PARSER: INDEX
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var parserCreateIndex = {

    //reIndex: /^create\s(\w*)?\s?index\s(\w)\son\s(\w)\s\(([\w\s,]+)\)?/i,
    reIndex: /^create (\w*)\s?index (\w+) on (\w+) \(([\w,]+)\)\s*;/i,

    matchIn: function (line) {
        "use strict";


        return line.match(this.reIndex);

    },
    matchOut: function (line) {
        "use strict";

        return true;

    },
    parser: function (buffer) {
        "use strict";

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Apenas uma linha
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        var index = buffer[0].match(this.reIndex);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Carrega uma tabela se não existir cria
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        var table = mysql.table(index[3]);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Salva
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        table.addIndex({
            type: index[1],
            indexName: index[2],
            columns: index[4].split(',')
        });


    }

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PARSER: REFERENCES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var parserRelations = {

    re1: /^alter table (\w+) add constraint (\w+)/i,
    re2: /^\s+foreign key \((\w+)\)/i,
    re3: /^\s+references (\w+) \((\w+)\) (?:(?:ON DELETE (RESTRICT|CASCADE|SET NULL|NO ACTION))|(?:ON UPDATE (RESTRICT|CASCADE|SET NULL|NO ACTION)))?/i,

    matchIn: function (line) {
        "use strict";

        return line.match(this.re1);

    },
    matchOut: function (line) {
        "use strict";

        //console.log(line);
        return line.match(this.re3);

    },
    parser: function (buffer) {
        "use strict";

        // Deve retornar 3 linhas //
        var rel1 = buffer[0].match(this.re1);
        var rel2 = buffer[1].match(this.re2);
        var rel3 = buffer[2].match(this.re3);


        var tableName = rel1[1];

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Carrega uma tabela se não existir cria
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        var table = mysql.table(tableName);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Salva
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        table.addRelation({
            foreignKeyName: rel1[2],
            foreignKey: rel2[1].split(','),
            referenceTable: rel3[1],
            referenceKey: rel3[2].split(','),
            onDelete: rel3[3],
            onUpdate: rel3[4]

        });


        //console.log(buffer);

    }

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PARSER: Insert
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var parserInsert = {

    reInsert: /^insert into (\w+) values\(([\w',]+)\) ;/i,

    matchIn: function (line) {
        "use strict";


        return line.match(this.reInsert);

    },
    matchOut: function (line) {
        "use strict";

        return true;

    },
    parser: function (buffer) {
        "use strict";

        var r = buffer[0].match(this.reInsert);
        var fixture = [];

        var tableName = r[1];

        // Explode string pela virgula, e depois remove o primeiro e último caractér (acentos
        r[2].split(',').forEach(function (i) {
            fixture.push(i.substr(1, i.length - 2));
        });


        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Carrega uma tabela se não existir cria
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        var table = mysql.table(tableName);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // Salva
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        table.addData(fixture);


    }

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ADAPTER MYSQL
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
mysql = Object.create(adapterBase, {
    parsers: {
        value: [
            parserCreateTable,
            parserCreateIndex,
            parserRelations,
            parserInsert
        ]
    },
    tables: {
        value: {},
        enumerable: true
    }
});


module.exports = mysql;