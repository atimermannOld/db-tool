# Descrição

Ferramentas diversas para trabalhar com dados como:
* Conversor de diagrama Dia para SQL
* Migração

# Sql Parser #

# Convert Tool #

# Model Validate #

# Data Migrate #

Esta ferramenta tem como objetivo gerenciar migração de banco de dados. O maior trinfu é criar os arquivo de migração automáticamente inspirado no South do Django, porém utilizando uma nova abordagem.

Ao inicia uma estrutura nova de migração teremos dois diretórios:

## Estrutura

**- source**

    Aqui vamos armazenar os arquivos de definição do banco de dados dia ou sql
    Deve estar no formato:
    [qualquer-nome]-[numero-sequencial-com-5-digitos].dia
    ou
    [qualquer-nome]-[numero-sequencial-com-5-digitos].sql


**- schema**

    Aqui ficarão armazenados todas as estruturas de banco de dado criado, cada estrutura terá seu firetório no formato:
    schema-[data]-[numero-sequencial-com-5-digitos]  exemplo: schema-20141122-00001

    Dentro vamos ter a estrutura em formato json, gerado pelo sql_parser com o título:
        migrantion-schema.json

    E o arquivo de definição de migração (TODO)

**- migration**

    Neste diretório, vamos salvar as migrações de dados, com backup, mapa de dados e gabarito de migração.
    Para cada migração será criado um novo diretório, dependendo do tamanho do banco pode ficar muito grande, recomenda-se gerenciar estes diretórios
    Ao contrário de outras arquiteturas, não teremos UP e DOWN, ao gerar migração dizemos de onde queremos partir e pra onde quermos ir, podendo ser UP ou DOWN e pra cada atividade desta será criado um novo diretório aqui




## Comandos:

**$ db-data-migrate init**

    Inicializa estrutura de diretórios, criar arquivos base
- **--path <caminho>**  Padrão: Diretório atual



**$ db-data-migrate create-schema <arquivo.dia|arquivo.sql>**

    Cria um novo esquema (snapshot) da estrutura do banco de dados no formato: schema-[data]-[numero-sequencial-com-5-digitos]
- **--path <caminho>**  Padrão: Diretório atual


**$ db-data-migrate create-migration <numero-do-schema-algo>**

    Gera uma nova migração. Se numero-do-schema-algo não for passado, assume mais recente
    É cria os gabarito para preenchimento, que serão utilizado na migração de dados
- **--path <caminho>**  Padrão: Diretório atual


**$ db-data-migrate run-migration <numero-da-migracao>**

    Executa migração.
- **--path <caminho>**  Padrão: Diretório atual